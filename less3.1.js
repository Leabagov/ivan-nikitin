//функции. модули и директивы require(), функция call вызывает сама себя.
  function test() {
    console.log("Привет, функция тест, проверка");
  }

test();


var printSomething = function() {
  console.log("Просто текст, самовызов функции")
};

printSomething();


function call(func) {
    func();
  }
  
  call(printSomething);

  // Вызываем код из файла less3.2
  var counter = require('./less3.2');

  console.log(counter([1, 3, 33, 344, 33, 6]));
