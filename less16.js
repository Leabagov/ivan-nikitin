/* использование данный, использование буддстрап, даные с формы отправляются на сервер.  */


var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.set('view engine', 'ejs');
  app.use('/public', express.static('public'));

  
 
app.get('/', function(req, res){
  res.render('less16');
});

app.get('/about', function(req, res){
  res.render('about');
});

app.post('/about', urlencodedParser, function(req, res){

  if (!req.body) return res.sendStatus(400)
  console.log(req.body);
  res.render('about-sucess');
});

app.get('/news/:id', function(req, res){
  var obj = {title: "Новость", id: 4, paragraphs: ['Параграф', "обычный текста", "Числа:5, 4, 5, 64, 44 , 44", 99] };
  res.render('news', {newsId: req.params.id, newParam:334, obj: obj});
});




app.listen(3000);
