/* статические файлы и промежуточное по*/






var express = require('express');
var app = express();
app.set('view engine', 'ejs');

 //подключаем статические файлы
  app.use('css', express.static('public'));

  
 
app.get('/', function(req, res){
  res.sendFile(__dirname + '/less14_html.html');
});

app.get('/about', function(req, res){
  res.sendFile(__dirname + '/less11_about.html');
});

app.get('/news/:id', function(req, res){
  var obj = {title: "Новость", id: 4, paragraphs: ['Параграф', "обычный текста", "Числа:5, 4, 5, 64, 44 , 44", 99] };
  res.render('news', {newsId: req.params.id, newParam:334, obj: obj});
});

app.listen(3000);
