//Изучение фреймворка Express


var express = require('express');

//переменная использует переменные express
var app = express();

app.get('/', function(req, res){
  res.send('This is Home');
});

app.get('/news', function(req, res){
  res.send('This is news');
});


//выводит страницу с id , в айди можно загружать файлы
app.get('/news/:name/:id', function(req, res){
  res.send('ID is -'  + req.params.name + '/' + req.params.id);
});

app.listen(3000);
