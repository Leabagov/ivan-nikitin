//Работа с потоками в Node JS, асинхронное чтение текста


/* Работа с потоками. функци позволяет загружать информацию частями, делит информация на блоки
 и записывает в новый файл. Хорошо оптимизирует передачу данныйх */

 var fs = require('fs');

/** 

 //берется текст из файла и выводится частями
 var myReadShort = fs.createReadStream(__dirname + '/less9_2.txt', 'utf8');
 myReadShort.on('data', function(chunk){
  console.log("Новые данные получены:\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" + chunk);
 
});

**/





 var fs = require('fs');

 var myReadShort = fs.createReadStream(__dirname + '/less9_2.txt', 'utf8');
 var myWriteShort = fs.createWriteStream(__dirname + '/news.txt')
 
 myReadShort.on('data', function(chunk){
   console.log("Новые данные получены:");
   myWriteShort.write(chunk);
 });
 

