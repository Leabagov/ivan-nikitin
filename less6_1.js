//написание чтение файла


var fs = require('fs');

//Прочитка файла, код не начинается пока не сработает прочитка
var file_readed = fs.readFileSync('less6_text.txt', 'utf8');
var message = "Привет, сообщение урок 6\n" + file_readed;

//создает и записывает содержимое в другой файл
fs.writeFileSync('some_new_file.txt', message);

//асинхроное чтение файла
fs.readFile('less6_text.txt', 'utf8', function(err, data) {
  console.log(data);
});

fs.writeFile('some.txt', 'hi its me,lesson 6', function(err, data) {});

console.log("Test");
