
//Работа с директориями


var fs = require('fs');
 //создает файл с тектом в папке
fs.mkdir('new-one', function(){
  fs.writeFile('./new-one/some_new.txt', 'привет мир', function(){
    console.log('урок 7 , все сработало, создалась новая папка new-one и в нее записался текстовый файл, с текстом "Привет мир" ');
  })
});


//удаляет файл в папке
/**
fs.unlink('./new-one/some_new.txt', function(){
    fs.rmdir("new-one", function() {});
});
**/


//создает новую папку < Синхроно
//fs.mkdirSync('new-one', function(){});

//удаляет новую папку< Асинхроно
//fs.rmdir('new-one', function(){});
